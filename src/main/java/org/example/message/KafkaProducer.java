package org.example.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaProducer {

    @Autowired
    public KafkaTemplate<String, String> kafkaTemplate;

    // topic untuk dikirimkan message nya
    private String topic = "topicSql";

    // method untuk kirim message
    public void send(String message) {
        // kafka template send itu minta 2 argument; topic nya dan message
        kafkaTemplate.send(topic, message);
    }
}
