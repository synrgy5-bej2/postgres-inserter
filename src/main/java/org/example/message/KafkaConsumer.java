package org.example.message;

import lombok.extern.slf4j.Slf4j;
import org.example.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class KafkaConsumer {

    // service yang akan dijalanin ketika consume data
    @Autowired
    MessageService messageService;

    // KafkaListener ini untuk pake bean kafkaListener yg ada di config
    // yang perlu di set minimal itu adalah topics;
    // argument method nya harus sesuai dengan deserializer yaitu String
    @KafkaListener(topics = "${kafka.topic.consume}", groupId = "${kafka.group.id}")
    public void listen(String payload) {
        log.info("payload : {}", payload);

        // panggil method untuk memproses data yg sudah di consume
        messageService.insertMessage(payload);
    }
}
