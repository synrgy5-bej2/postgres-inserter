package org.example.controller;

import org.example.service.DemoAsync;
import org.example.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.ExecutionException;

@RestController
public class DemoController {

    @Autowired
    public MessageService messageService;

    @Autowired
    public DemoAsync demoAsync;

    @GetMapping("/demo-async")
    public String async1() throws InterruptedException {
        demoAsync.async1();
        return "async coy";
    }

    @GetMapping("/demo-async-2")
    public String async2() throws InterruptedException, ExecutionException {
        Thread.sleep(2000);
        return demoAsync.async2().get();
    }

    @GetMapping("/demo-sync")
    public String sync1() throws InterruptedException {
        demoAsync.sync1();
        return "sync";
    }

    @GetMapping("/demo1")
    public String demo1() {
        return messageService.demo1();
//        return "Mantuls";
    }

    @GetMapping("/demo2")
    public String demo2() {
        return messageService.demo2();
    }

    @GetMapping("/demo3")
    public String demo3() {
        return messageService.demo3();
    }

    @GetMapping("/demo4")
    public String demo4() {
        return messageService.demo4();
    }

    @GetMapping("/demo5")
    public String demo5() {
        return messageService.demo5();
    }
}
