package org.example.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Service
public class DemoAsync {

    @Async
    public void async1() throws InterruptedException {
        Thread.sleep(5000);
    }

    @Async
    public Future<String> async2() throws InterruptedException {
        Thread.sleep(5000);
        return new CompletableFuture<>();
    }

    public void sync1() throws InterruptedException {
        Thread.sleep(5000);
    }
}
