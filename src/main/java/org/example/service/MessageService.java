package org.example.service;

public interface MessageService {

    void insertMessage(String message);

    String demo1();

    String demo2();

    String demo3();

    String demo4();

    String demo5();

    String demoAsync() throws InterruptedException;
}
