package org.example.service;

import lombok.extern.slf4j.Slf4j;
import org.example.model.Message;
import org.example.repository.MessageRepository;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageRepository messageRepository;

    @Override
    public void insertMessage(String message) {
        Message msg = new Message();
        msg.setMessage(message);
        messageRepository.save(msg);
    }

    // readOnly itu memang mempercepat proses read, tapi gabisa klo dipake buat insert/update/delete
    @Transactional(readOnly = true)
    @Override
    public String demo1() {
        Message message = new Message();
        message.setMessage("transactional");
        // insert
        messageRepository.save(message);
        // select
        Message hasil = messageRepository.findByMessage(message.getMessage());
        System.out.println(hasil.getMessage());
        // delete
        messageRepository.deleteById(hasil.getId());
        return "OK";
    }

    // rollbackFor kita mengeskplisitkan class exception tertentu jika terjadi akan di rollback
    // soalnya kadang2 ada exception class yg tidak memberhentikan proses dalam method
    @Transactional(rollbackFor = NullPointerException.class)
    @Override
    public String demo2() {
            Message message = new Message();
            message.setMessage("transactional null dianggap");
            // insert
            messageRepository.save(message);
            // select
            Message hasil = messageRepository.findByMessage(message.getMessage());
            hasil = null;
            System.out.println(hasil.getMessage());
            Integer number = 1/0;
            // delete
            messageRepository.deleteById(hasil.getId());
        return "OK";
    }

    // noRollbackFor, tidak melakukan rollback jika terjadi class exception yg dimasukkan
    @Transactional(noRollbackFor = NullPointerException.class)
    @Override
    public String demo3() {
        Message message = new Message();
        message.setMessage("transactional lanjay");
        // insert
        messageRepository.save(message);
        // select
        Message hasil = messageRepository.findByMessage(message.getMessage());
        System.out.println(hasil.getMessage());
        hasil = null;
        // delete
        messageRepository.deleteById(hasil.getId());
        return "OK";
    }

    @Override
    public String demo4() {
        Message message = new Message();
        message.setMessage("tanpa transactional");
        // insert
        messageRepository.save(message);
        // select
        Message hasil = messageRepository.findByMessage(message.getMessage());
        System.out.println(hasil.getMessage());
        hasil = null;
        // delete
        messageRepository.deleteById(hasil.getId());
        return "OK";
    }

    @Transactional(noRollbackFor = NullPointerException.class)
    @Override
    public String demo5() {
        Message message = new Message();
        message.setMessage("tanpa transactional");
        // insert
        messageRepository.save(message);
        // select
        Message hasil = messageRepository.findByMessage(message.getMessage());
        hasil = null;
        System.out.println(hasil.getMessage());
        Integer nilai = 1/0;
        // delete
        messageRepository.deleteById(hasil.getId());
        return "OK";
    }

    @Override
    public String demoAsync() throws InterruptedException {
        Thread.sleep(2000);
        return "sudah manchaap";
    }
}
