FROM openjdk:8-jdk-alpine

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} handson.jar
ENTRYPOINT ["java","-jar","/handson.jar"]